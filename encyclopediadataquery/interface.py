from querypipeline import *
from luigi.tools import deps


@inherits(EncyclopediaParams)
class MainTask(luigi.WrapperTask):
    ms_set = FilePathParameter(description="Path to a local folder containing MS files or a file listing "
                                            "paths to local or S3 MS files")
    output_path = FilePathParameter(description="Path to download processing output to")

    def requires(self):
        irf = self.clone(ImportRawFilesSkyline, ms_path_list=self._get_ms_paths())
        yield irf
        yield DownloadAll(seed_task=irf, output_path=self.output_path)

    def _get_ms_paths(self):
        if not os.path.exists(self.ms_set):
            raise ValueError("ms file path {} does not exist".format(self.ms_set))
        if os.path.isdir(self.ms_set):
            formats = ("mzml", "mzxml", "mz5", "mgf", "text", "ms2", "cms2", "raw", "d", "wiff")
            return [os.path.abspath(os.path.join(self.ms_set, f))
                    for f in os.listdir(self.ms_set) if os.path.splitext(f)[-1][1:].lower() in formats]
        # path is a text file with an mzml file path on each line
        with open(self.ms_set) as fin:
            return [line.strip() for line in fin.readlines()]


class DownloadAll(luigi.WrapperTask):
    seed_task = SpecificTaskParameter()
    output_path = FilePathParameter()

    def requires(self):
        upstream = deps.find_deps(self.seed_task, None)
        for t in upstream:
            if not isinstance(t, ECSTask):
                continue
            if len(t.downloadable_outputs) > 0:
                yield DownloadOutputs(task_object=t, output_path=self.output_path)


if __name__ == '__main__':
    # logging.getLogger('encyclopedia-task').addHandler(h)
    luigi.run()
